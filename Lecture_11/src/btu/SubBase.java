package btu;

public class SubBase extends Base {

    @Override
    public void method(){
        System.out.println("SubBase");
    }

    //Override
    @Deprecated
    public void method1(){
        System.out.println("SubBase Method 1");
    }
}
