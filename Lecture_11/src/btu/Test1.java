package btu;

public class Test1 <GPA, NAME> {

    Object x;
    GPA gpa;
    NAME name;


    public void method1(Integer time){
        System.out.println(time.getClass().getTypeName());
    }

    public void method2(Object time){
        System.out.println(time.getClass().getTypeName());
    }

    public <TIME> void method3(TIME t){
        System.out.println(t.getClass().getTypeName());
    }
}
