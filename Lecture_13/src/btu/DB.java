package btu;

import java.sql.*;

public class DB {
    Connection connection;
    String dbConnection = "jdbc:mysql://localhost:3306/javabtu4";

    public DB(){
        try {
            connection = DriverManager.getConnection(dbConnection, "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id){
        String deleteQuery = "DELETE FROM users WHERE id=?";
        try {
            PreparedStatement pr = connection.prepareStatement(deleteQuery);
            pr.setInt(1, id);
            pr.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void insert(String username, String password, String email){
        String insertQuary = "INSERT INTO users(username, password, email) VALUES (?, ?, ?)";
        try {
            PreparedStatement prS = connection.prepareStatement(insertQuary);
            prS.setString(1, username);
            prS.setString(2, password);
            prS.setString(3, email);
            prS.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public void select(){
        String selectQuery = "SELECT username, password FROM users";
        try {
            Statement stm = connection.createStatement();
            ResultSet resultSet =  stm.executeQuery(selectQuery);
            while (resultSet.next()) {
//            resultSet.next();
//            resultSet.next();
                System.out.print(resultSet.getString("username") + " - ");
                System.out.println(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
