package btu;

public interface Test1 {
    public void test1_1();
    public int test1_2();
    public int test1_3(int x, String n);

    public int x = 87;
    String y = "9";
    private void test1_4(){
        System.out.println("test1_4");
    }

    public default void test1_5(){
        test1_4();
        int x = 98;
        System.out.println(this.x);
        System.out.println();
    }
}
