package btu;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int [] m1 = {3, 4, 4};
        Object [] m2 = {3, 4.6, "Java", true};
        ArrayList a1 = new ArrayList();
        ArrayList <Object> a2 = new ArrayList<Object>();
        a1.add(3);
        a1.addAll(Arrays.asList(m2));
//        a1.addAll(Arrays.asList(m1));
//        System.out.println(a1);
        a2.add(3);
        a2.add(2);
        a2.add(4);
        a2.add(1);
//        System.out.println(a2);
//        System.out.println(a2.get(2));
//        System.out.println(a2.indexOf(9));
//        System.out.println(a2.size());
//        a2.remove(2);
//        System.out.println(a2);
//        a2.remove((Integer)2);
//        System.out.println(a2);
//        a2.add(1, "Java");
//        System.out.println(a2);
        Set s = new TreeSet();
        s.add(16);
        s.add(3);
        s.add(2);
        s.add(2);
        s.add(23);
        s.add(-5);
        s.add(9);
        s.add(9);
        System.out.println(s);
    }
}
