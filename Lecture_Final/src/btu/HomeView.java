package btu;

import com.sun.tools.jconsole.JConsoleContext;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class HomeView implements Initializable {
    @FXML
    public VBox container;

    public HomeView(){

    }

    public void insert(){
        try {
            Stage stage = new Stage();
            stage.setTitle("INSERT");
            Parent parent = FXMLLoader.load(getClass().getResource("InsertView.fxml"));
            Scene scene  = new Scene(parent);
            stage.setScene(scene);
            stage.show();
        }catch (Exception e){

        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Db db = new Db();
        ResultSet resultSet = db.select();
        System.out.println(resultSet);
        container.setSpacing(15);
//        try {
//            System.out.println(resultSet.next());
//            System.out.println(resultSet.getString("email"));
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        try{
            while(resultSet.next()){
                System.out.println("12");
                System.out.println(resultSet.getString("username"));
                Label username = new Label(resultSet.getString("username"));
                container.getChildren().add(username);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
