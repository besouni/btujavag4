package btu;

public class Runtime {
    public Runtime(){
        System.out.println("Constructor is running!!!");
    }

    public void Test(){
        int x = 9, y = 0;
        int m[] = {4, 5, 56};
        String s = "Java";
        try{
            System.out.println("Before try");
            System.out.println(s.charAt(10));
            System.out.println("Try - >"+m[0]);
            System.out.println("Try -> "+x/y);
            System.out.println("After try");
        }catch(Exception e){
            System.out.println("Catch -> Exception "+e.getMessage());
        }finally {
            System.out.println("This is final block");
        }
//        try{
//            System.out.println("Before try");
//            System.out.println(s.charAt(10));
//            System.out.println("Try - >"+m[0]);
//            System.out.println("Try -> "+x/y);
//            System.out.println("After try");
//        }catch(ArithmeticException e){
//            System.out.println("Catch -> Arithmetic "+e.getMessage());
//        }catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("Catch ->  Array"+e.getMessage());
//        }finally {
//            System.out.println("This is final block");
//        }

        System.out.println("After Exception");
    }
}
