package btu;

public class Person {
    String name = "Anna";
    String lastname;
    int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int _age) {
        age = _age;
    }

    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                '}';
    }

    public Person() {
        System.out.println("Constructor is running!!!!");
    }

    public Person(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }
}
