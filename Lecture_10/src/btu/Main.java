package btu;

import javax.swing.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) {
	// write your code here
         Server server = new Server();
         Thread s = new Thread(server);
         s.start();

         Client c = new Client();
         c.start();
//        System.out.println(InetAddress.getLoopbackAddress());
//        try {
//            System.out.println(InetAddress.getLocalHost());
//            System.out.println(InetAddress.getByName("www.w3schools.com"));
//            System.out.println(InetAddress.getByName("www.btu.edu.ge"));
//            System.out.println(InetAddress.getByName("localhost"));
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
    }
}
