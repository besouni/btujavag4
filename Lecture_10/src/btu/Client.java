package btu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread{
    Socket socket;
    String message;
    ObjectOutputStream objectOutputStream;
    ObjectInputStream objectInputStream;

    @Override
    public void run() {
        try {
            while (true) {
                socket = new Socket(InetAddress.getByName("localhost"), 8080);
                Scanner scanner = new Scanner(System.in);
                message = scanner.nextLine();
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(message);
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Server: " + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
