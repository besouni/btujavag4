package btu;

import thread.Thread1;
import threadTest.MyResources;
import threadTest.ThreadTest;

public class Main {

    public static void main(String[] args) {
	// write your code here

        MyResources myResources  = new MyResources();
//        myResources.method(100, 5, "Main");

        ThreadTest threadTest = new ThreadTest(myResources);
        Thread thread = new Thread(threadTest);
        thread.start();

        myResources.method(100, 5, "Main");

//        Thread.currentThread().setName("My main thread");
//        System.out.println(Thread.currentThread().getName());
//        Thread1 th1 = new Thread1();
//        th1.start();
//
//        for(int i=0; i<10; i++){
//            System.out.println("Main - "+i);
//        }

//        System.out.println(th1.getName());
//        th1.method();
//        System.out.println("main finished");
    }
}
