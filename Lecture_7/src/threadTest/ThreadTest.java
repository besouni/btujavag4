package threadTest;

public class ThreadTest implements Runnable {
    MyResources myResources;

    public ThreadTest(MyResources myResources){
            this.myResources = myResources;
    }

    @Override
    public void run() {
        myResources.method(200, 5, "Thread");
    }
}
